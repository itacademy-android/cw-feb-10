package itacademy.com.project008;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class ListActivity extends AppCompatActivity implements View.OnClickListener {
    private SQLiteHelper sqLiteHelper;
    private ArrayList<UserModel> userList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        sqLiteHelper = new SQLiteHelper(this);

        Button btnSave = findViewById(R.id.btnSave);
        btnSave.setOnClickListener(this);

        ListView listView = findViewById(R.id.listView);

        userList = new ArrayList<>();
        for (int i = 10; i < 20; i++) {
            UserModel model = new UserModel();
            model.setId(i);
            model.setName("John Smith " + i);
            model.setNumber(99995555);
            userList.add(model);
        }

        UserAdapter adapter = new UserAdapter(this, userList);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(ListActivity.this, ShowActivity.class);
                intent.putExtra("model", (Parcelable) userList.get(i));
                startActivity(intent);
            }
        });

    }

    @Override
    public void onClick(View view) {
        sqLiteHelper.saveUserList(userList);
    }
}
