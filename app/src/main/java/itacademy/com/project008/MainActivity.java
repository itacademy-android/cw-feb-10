package itacademy.com.project008;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import java.io.Serializable;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private Menu menu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnModel = findViewById(R.id.btnModel);
        Button btnList = findViewById(R.id.btnList);
        Button btnSQLite = findViewById(R.id.btnSqlite);

        btnModel.setOnClickListener(this);
        btnList.setOnClickListener(this);
        btnSQLite.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnModel:
                UserModel model = new UserModel();
                model.setId(10);
                model.setName("John");
                model.setNumber(1999555);

                Intent intent = new Intent(MainActivity.this, ModelActivity.class);
                intent.putExtra("model", (Serializable) model);
                startActivity(intent);
                break;
            case R.id.btnList:
                Intent listIntent = new Intent(MainActivity.this, ListActivity.class);
                startActivity(listIntent);
                break;
            case R.id.btnSqlite:
                getMenuInflater().inflate(R.menu.menu, menu);
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
}
