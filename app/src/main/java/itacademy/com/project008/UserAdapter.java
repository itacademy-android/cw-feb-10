package itacademy.com.project008;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class UserAdapter extends ArrayAdapter<UserModel> {

    private Context context;

    public UserAdapter(Context context, ArrayList<UserModel> list) {
        super(context, 0, list);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.user_list_item, parent, false);
            holder = new ViewHolder();
            holder.tvId = convertView.findViewById(R.id.tvId);
            holder.tvName = convertView.findViewById(R.id.tvName);
            holder.tvNumber = convertView.findViewById(R.id.tvNumber);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        UserModel model = getItem(position);

        holder.tvId.setText(String.valueOf(model.getId()));
        holder.tvName.setText(model.getName());
        holder.tvNumber.setText(String.valueOf(model.getNumber()));

        return convertView;
    }

    private class ViewHolder {
        TextView tvId, tvName, tvNumber;
    }

}
