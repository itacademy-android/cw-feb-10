package itacademy.com.project008;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ShowActivity extends AppCompatActivity {

    private TextView tvName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show);
        tvName = findViewById(R.id.tvName);

        Intent intent = getIntent();
//        String name = intent.getStringExtra("name");
        UserModel model = intent.getParcelableExtra("model");

//        String name = String.format("%1s %2s", model.getName(), model.getNumber());
        String name = String.format(getString(R.string.name), model.getName(),
                String.valueOf(model.getNumber()));
        tvName.setText(name);

    }
}
