package itacademy.com.project008;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ModelActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_model);

        Intent intent = getIntent();
        UserModel userModel = (UserModel) intent.getSerializableExtra("model");

        TextView tvId = findViewById(R.id.tvId);
        TextView tvName = findViewById(R.id.tvName);
        TextView tvNumber = findViewById(R.id.tvNumber);

        tvId.setText(String.valueOf(userModel.getId()));
        tvName.setText(userModel.getName());
        tvNumber.setText(String.valueOf(userModel.getNumber()));
    }
}
